from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


GENDER = (('man', 'Man'), ('woman', 'Woman'))


class UserProfile(models.Model):
    user = models.OneToOneField(User, null=True, related_name="profile",
                                verbose_name=_('User'), on_delete=models.CASCADE)
    phone = models.PositiveIntegerField(
        null=True, blank=True, verbose_name=_('Phone'))
    gender = models.CharField(
        max_length=40, blank=True, verbose_name=_('Gender'), choices=GENDER)
    email_is_verified = models.BooleanField(
        default=False, verbose_name=_('Email is verified'))

    class Meta:
        verbose_name = _('User profile')
        verbose_name_plural = _('User profiles')

    def __str__(self):
        return "User profile: %s" % self.user.username