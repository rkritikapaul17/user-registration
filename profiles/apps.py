from django.apps import AppConfig
from django.urls import reverse_lazy as _


class ProfilesConfig(AppConfig):
    name = 'profiles'

    def ready(self):
        from profiles import signals