# from django.contrib.auth.forms import UserCreationForm  
# from django.contrib.auth.models import User
# class SignUpForm(UserCreationForm):  
#         class Meta:  
#             model = User  
#             fields = ('email', 'first_name', 'last_name', 'username')

from allauth.account.forms import SignupForm
from django import forms

class CustomSignupForm(SignupForm):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')
    address = forms.CharField(max_length=30, label='Address')
    zipcode = forms.CharField(max_length=30, label='zipcode')
    state = forms.CharField(max_length=30, label='State')
    country = forms.CharField(max_length=30, label='Country')
    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.address = self.cleaned_data['address']
        user.zipcode = self.cleaned_data['zipcode']
        user.state = self.cleaned_data['state']
        user.country = self.cleaned_data['country']
        user.save()
        return user